# console.go

这是一个参考 [Firebug Console API](http://getfirebug.com/wiki/index.php/Console_API) 的 fmt.Print 封装。

避免把大量时间都花费在输入前缀、空格、换行符等操作上。

# 获取

可以直接用`go get`获取，但不建议这么做。

由于可能会被经常使用，避免繁琐冗余的引入名，推荐使用以下命令行来获取：

```
cd $GOPATH/src
git clone https://github.com/yulon/console.go.git
mv console.go console
go install console
```
