package console

import (
	"fmt"
	"time"
	"strconv"
)

func Log(obj ...interface{}) {
	for i := 0; i < len(obj); i++ {
		fmt.Print(obj[i], " ")
	}
	fmt.Print("\n")
}

func Warn(obj ...interface{}) {
	fmt.Print("[!] ")
	Log(obj)
}

func Info(obj ...interface{}) {
	fmt.Print("[i] ")
	Log(obj)
}

func Error(obj ...interface{}) {
	fmt.Print("[X] ")
	Log(obj)
}

var timeMap map[string]int64 = make(map[string]int64)

func Time(name string) {
	timeMap[name] = time.Now().UnixNano()
}

func TimeEnd(name string) {
	old, ok := timeMap[name]
	if ok {
		delete(timeMap, name)
		fmt.Print("[🕒] " + name + ": " + strconv.FormatFloat(float64(time.Now().UnixNano() - old) / 1000000, 'f', 3, 64) + "ms\n")
	}else{
		Error("[🕒] Can't find \"" + name + "\"\n")
	}
}